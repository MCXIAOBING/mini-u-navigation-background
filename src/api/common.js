import request from '@/utils/request'

// 获取图片列表
export function getImgList(params = {}) {
  return request({
    url: '/Image/imgList',
    method: 'get',
    params,
  })
}

// 删除图片
export function removeImg(id) {
  return request({
    url: '/Image/removeImg',
    method: 'post',
    data: {
      id,
    },
  })
}

// 获取七牛云上传token
export function getQiniuToken(params = {}) {
  return request({
    url: '/Uploadfile/QiniuToken',
    method: 'get',
    params,
  })
}
