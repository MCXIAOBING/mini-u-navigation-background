import request from '@/utils/request'

// 添加路由
export function addRoute(data) {
  return request({
    url: '/Systemweb/add',
    method: 'post',
    data,
  })
}

// 获取路由列表
export function getRouteList(params = {}) {
  return request({
    url: '/Systemweb/articleList',
    method: 'get',
    params,
  })
}

// 获取路由列表 -- 精简 ，选择用
export function getRouteLite(params = {}) {
  return request({
    url: '/Systemweb/cateList',
    method: 'get',
    params,
  })
}

// 修改状态与删除
export function editRouteType(data) {
  return request({
    url: '/Systemweb/editType',
    method: 'post',
    data,
  })
}

// 获取路由详情
export function getRouteDetail(id) {
  return request({
    url: '/Systemweb/detail',
    method: 'get',
    params: {
      id,
    },
  })
}

// 修改路由
export function editRoute(data) {
  return request({
    url: '/Systemweb/edit',
    method: 'post',
    data,
  })
}

// 获取权限列表
export function getPermissionList(params = {}) {
  return request({
    url: '/Systemwebpower/articleList',
    method: 'get',
    params,
  })
}

// 添加权限
export function addPermission(data) {
  return request({
    url: '/Systemwebpower/add',
    method: 'post',
    data,
  })
}

// 修改权限状态与删除
export function editPermissionType(data) {
  return request({
    url: '/Systemwebpower/editType',
    method: 'post',
    data,
  })
}

// 获取权限详情
export function getPermissionDetail(id) {
  return request({
    url: '/Systemwebpower/detail',
    method: 'get',
    params: {
      id,
    },
  })
}

// 修改权限
export function editPermission(data) {
  return request({
    url: '/Systemwebpower/edit',
    method: 'post',
    data,
  })
}

// 获取角色列表
export function getRoleList(params = {}) {
  return request({
    url: '/Systemwebrole/articleList',
    method: 'get',
    params,
  })
}

// 添加角色
export function addRole(data) {
  return request({
    url: '/Systemwebrole/add',
    method: 'post',
    data,
  })
}

// 修改角色状态与删除
export function editRoleType(data) {
  return request({
    url: '/Systemwebrole/editType',
    method: 'post',
    data,
  })
}

// 获取角色详情
export function getRoleDetail(id) {
  return request({
    url: '/Systemwebrole/detail',
    method: 'get',
    params: {
      id,
    },
  })
}

// 修改角色
export function editRole(data) {
  return request({
    url: '/Systemwebrole/edit',
    method: 'post',
    data,
  })
}

// 获取账号列表
export function getAccountList(params = {}) {
  return request({
    url: '/Systemadmin/articleList',
    method: 'get',
    params,
  })
}

// 添加账号
export function addAccount(data) {
  return request({
    url: '/Systemadmin/add',
    method: 'post',
    data,
  })
}

// 修改账号状态与删除
export function editAccountType(data) {
  return request({
    url: '/Systemadmin/editType',
    method: 'post',
    data,
  })
}

// 获取账号详情
export function getAccountDetail(id) {
  return request({
    url: '/Systemadmin/detail',
    method: 'get',
    params: {
      id,
    },
  })
}

// 修改账号
export function editAccount(data) {
  return request({
    url: '/Systemadmin/edit',
    method: 'post',
    data,
  })
}

// // 获取接口列表
// export function getApiList(params = {}) {
//   return request({
//     url: '/System/articleList',
//     method: 'get',
//     params,
//   })
// }

// 添加接口
export function addApi(data) {
  return request({
    url: '/System/add',
    method: 'post',
    data,
  })
}

// 修改接口状态与删除
export function editApiType(data) {
  return request({
    url: '/System/editType',
    method: 'post',
    data,
  })
}

// 获取接口详情
export function getApiDetail(id) {
  return request({
    url: '/System/articleList',
    method: 'get',
    params: {
      id,
    },
  })
}

// 修改接口
export function editApi(data) {
  return request({
    url: '/System/edit',
    method: 'post',
    data,
  })
}

// 获取控制器及方法-- 控制器
export function getApis(params = {}) {
  return request({
    url: '/System/cList',
    method: 'get',
    params,
  })
}

// 获取使用操作方法
export function getMethod(params = {}) {
  return request({
    url: '/System/useMethod',
    method: 'get',
    params,
  })
}
