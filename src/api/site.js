import request from '@/utils/request'

// 站点列表
export function getSiteList(params = {}) {
  return request({
    url: '/Site/siteList',
    method: 'get',
    params,
  })
}

// 添加站点
export function addSite(data) {
  return request({
    url: '/Site/add',
    method: 'post',
    data,
  })
}

// 站点详情
export function getSiteDetail(id) {
  return request({
    url: '/Site/detail',
    method: 'get',
    params: {
      id
    },
  })
}

// 编辑站点
export function editSite(data) {
  return request({
    url: '/Site/edit',
    method: 'post',
    data,
  })
}

// 编辑站点状态
export function setSiteStatus(data) {
  return request({
    url: '/Site/setStatus',
    method: 'post',
    data,
  })
}
