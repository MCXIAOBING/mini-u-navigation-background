/**
 * @copyright
 * @description 路由守卫，目前两种模式：all模式与intelligence模式
 */
import router from '@/router'
import store from '@/store'
import VabProgress from 'nprogress'
import 'nprogress/nprogress.css'
import getPageTitle from '@/utils/pageTitle'
import { authentication, loginInterception, routesWhiteList, progressBar, recordRoute } from './settings'

VabProgress.configure({
  easing: 'ease',
  speed: 500,
  trickleSpeed: 200,
  showSpinner: false,
})
router.beforeResolve(async (to, from, next) => {
  if (progressBar) VabProgress.start()
  let hasToken = store.getters['user/accessToken']
  if (!loginInterception) hasToken = true
  if (hasToken) {
    if (to.path === '/login') {
      next({ path: '/' })
      if (progressBar) VabProgress.done()
    } else {
      const hasPermissions = store.getters['user/permissions'] && store.getters['user/permissions'].length > 0
      if (hasPermissions) {
        next()
      } else {
        try {
          const permissions = await store.dispatch('user/getInfo')
          let accessRoutes = []
          if (authentication === 'intelligence') {
            // console.log(permissions)
            // return
            accessRoutes = await store.dispatch('routes/setRoutes', permissions)
          } else if (authentication === 'all') {
            accessRoutes = await store.dispatch('routes/setAllRoutes')
            // console.log('后端返回权限')
            // console.log('-----处理好的权限列表-----')
            // console.log(accessRoutes)
          }
          router.addRoutes(accessRoutes)
          next({ ...to, replace: true })
        } catch (err) {
          console.log('出错了，错误如下👇👇👇')
          console.log(err)
          await store.dispatch('user/resetAccessToken')
          if (progressBar) VabProgress.done()
        }
      }
    }
  } else {
    if (routesWhiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      if (recordRoute) {
        next(`/login?redirect=${to.path}`)
      } else {
        next('/login')
      }

      if (progressBar) VabProgress.done()
    }
  }
  document.title = getPageTitle(to.meta.title)
})
router.afterEach(() => {
  if (progressBar) VabProgress.done()
})
