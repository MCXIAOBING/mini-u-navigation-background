import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import VCharts from 'v-charts'
import axios from 'axios'
import './plugins'
import './styles/global.scss'
import './utils/filters'
import noData from '@/components/NoData'
import pagination from '@/components/Pagination'
import uploadImg from '@/components/UploadImg'
import imgManager from '@/components/ImgManager'
import VueClipboard from 'vue-clipboard2'
import JsonViewer from 'vue-json-viewer'

Vue.use(VueClipboard)
Vue.use(VCharts)
Vue.use(JsonViewer)
Vue.prototype.$axios = axios
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock/static')
//   mockXHR()
// }
Vue.component('noData', noData)
Vue.component('pagination', pagination)
Vue.component('uploadImg', uploadImg)
Vue.component('imgManager', imgManager)

Vue.config.productionTip = false

new Vue({
  el: '#vue-admin-beautiful',
  router,
  store,
  render: h => h(App),
})
