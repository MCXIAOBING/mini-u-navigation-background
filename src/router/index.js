/**
 * @copyright
 * @description router全局配置，如有必要可分文件抽离，其中asyncRoutes只有在intelligence模式下才会用到，vip文档中已提供路由的基础图标与小清新图标的配置方案，请仔细阅读
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layouts/index.vue'
import SubLayout from '@/layouts/SubLayout.vue'
import EmptyLayout from '@/layouts/EmptyLayout'
import { routerMode } from '@/config/settings'

Vue.use(VueRouter)
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true,
  },
  {
    path: '/401',
    name: '401',
    component: () => import('@/views/401'),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true,
  },
  {
    path: '/personalCenter',
    component: Layout,
    hidden: true,
    redirect: 'personalCenter',
    children: [
      {
        path: 'personalCenter',
        name: 'PersonalCenter',
        component: () => import('@/views/personalCenter/index'),
        meta: {
          title: '个人中心',
        },
      },
    ],
  },
]

export const asyncRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: 'index',
    meta: {
      title: '概况',
      fullTitle: '概况',
      icon: 'home',
    },
    children: [
      {
        path: '/index-t',
        component: SubLayout,
        meta: {
          title: '概况',
        },
        children: [
          {
            path: '/index',
            name: 'index',
            component: () => import('@/views/index/index'),
            meta: {
              title: '概况',
            },
          },
        ],
      },
    ],
  },
  {
    path: '/site',
    component: Layout,
    redirect: 'siteList',
    meta: {
      title: '站点',
      fullTitle: '站点中心',
      icon: 'hashtag',
    },
    children: [
      {
        path: '/site-t',
        component: SubLayout,
        meta: {
          title: '站点管理',
        },
        children: [
          {
            path: '/siteList',
            name: 'siteList',
            component: () => import('@/views/site/list/index'),
            meta: {
              title: '站点列表',
            },
          },
          {
            path: '/addSite',
            name: 'addSite',
            hidden: true,
            component: () => import('@/views/site/list/add'),
            meta: {
              title: '添加站点',
            },
          },
          {
            path: '/editSite',
            name: 'editSite',
            hidden: true,
            component: () => import('@/views/site/list/edit'),
            meta: {
              title: '编辑站点',
            },
          },
        ],
      },
      {
        path: '/cate-t',
        component: SubLayout,
        meta: {
          title: '分类管理',
        },
        children: [
          {
            path: '/siteCategory',
            name: 'siteCategory',
            component: () => import('@/views/site/category/index'),
            meta: {
              title: '站点分类',
            },
          },
          {
            path: '/addCategory',
            name: 'addCategory',
            hidden: true,
            component: () => import('@/views/site/category/add'),
            meta: {
              title: '添加站点分类',
            },
          },
          {
            path: '/editCategory',
            name: 'editCategory',
            hidden: true,
            component: () => import('@/views/site/category/edit'),
            meta: {
              title: '编辑站点分类',
            },
          },
        ],
      },
    ],
  },
  {
    path: '/function',
    component: Layout,
    redirect: '/article',
    alwaysShow: true,
    meta: {
      title: '功能',
      icon: 'desktop',
      fullTitle: '其他功能',
    },
    children: [
      {
        path: '/article-t',
        component: SubLayout,
        meta: {
          title: '文章',
        },
        children: [
          {
            path: '/article',
            name: 'article',
            component: () => import('@/views/func/article/index'),
            meta: {
              title: '文章列表',
              noKeepAlive: true,
            },
          },
          // {
          //   path: '/errLogList',
          //   name: 'errLogList',
          //   component: () => import('@/views/system/errLogs/index'),
          //   meta: {
          //     title: '错误日志',
          //     noKeepAlive: true,
          //   },
          // },
        ],
      },
      {
        path: '/tools-t',
        component: SubLayout,
        meta: {
          title: '工具',
        },
        children: [
          {
            path: '/tools',
            name: 'tools',
            component: () => import('@/views/func/tools/index'),
            meta: {
              title: '推荐工具',
              noKeepAlive: true,
            },
          },
          // {
          //   path: '/errLogList',
          //   name: 'errLogList',
          //   component: () => import('@/views/system/errLogs/index'),
          //   meta: {
          //     title: '错误日志',
          //     noKeepAlive: true,
          //   },
          // },
        ],
      },
      {
        path: '/feedback-t',
        component: SubLayout,
        meta: {
          title: '反馈',
        },
        children: [
          {
            path: '/feedback',
            name: 'feedback',
            component: () => import('@/views/func/feedback/index'),
            meta: {
              title: '反馈内容',
              noKeepAlive: true,
            },
          },
          // {
          //   path: '/errLogList',
          //   name: 'errLogList',
          //   component: () => import('@/views/system/errLogs/index'),
          //   meta: {
          //     title: '错误日志',
          //     noKeepAlive: true,
          //   },
          // },
        ],
      },
    ],
  },
  {
    path: '/permissions',
    component: Layout,
    redirect: '/accountList',
    alwaysShow: true,
    meta: {
      title: '权限',
      icon: 'feather',
      fullTitle: '权限中心',
    },
    children: [
      {
        path: '/account-t',
        component: SubLayout,
        meta: {
          title: '员工管理',
        },
        children: [
          {
            path: '/accountList',
            name: 'accountList',
            component: () => import('@/views/permissions/account/index'),
            meta: {
              title: '账号列表',
              noKeepAlive: true,
            },
          },
          {
            path: '/addAccount',
            name: 'addAccount',
            hidden: true,
            component: () => import('@/views/permissions/account/add'),
            meta: {
              title: '添加账号',
              noKeepAlive: true,
            },
          },
          {
            path: '/editAccount',
            name: 'editAccount',
            hidden: true,
            component: () => import('@/views/permissions/account/edit'),
            meta: {
              title: '编辑账号',
              noKeepAlive: true,
            },
          },
          {
            path: '/rolesList',
            name: 'rolesList',
            component: () => import('@/views/permissions/roles/index'),
            meta: {
              title: '角色列表',
              noKeepAlive: true,
            },
          },
          {
            path: '/addRole',
            name: 'addRole',
            hidden: true,
            component: () => import('@/views/permissions/roles/add'),
            meta: {
              title: '添加角色',
              noKeepAlive: true,
            },
          },
          {
            path: '/editRole',
            name: 'editRole',
            hidden: true,
            component: () => import('@/views/permissions/roles/edit'),
            meta: {
              title: '编辑角色',
              noKeepAlive: true,
            },
          },
        ],
      },
      {
        path: '/permissions-t',
        component: SubLayout,
        meta: {
          title: '权限管理',
        },
        children: [
          {
            path: '/permissionList',
            name: 'permissionList',
            component: () => import('@/views/permissions/permission/index'),
            meta: {
              title: '权限列表',
              noKeepAlive: true,
            },
          },
          {
            path: '/addPermission',
            name: 'addPermission',
            hidden: true,
            component: () => import('@/views/permissions/permission/add'),
            meta: {
              title: '添加权限',
              noKeepAlive: true,
            },
          },
          {
            path: '/editPermission',
            name: 'editPermission',
            hidden: true,
            component: () => import('@/views/permissions/permission/edit'),
            meta: {
              title: '编辑权限',
              noKeepAlive: true,
            },
          },
          {
            path: '/addApi',
            name: 'addApi',
            hidden: true,
            component: () => import('@/views/permissions/api/add'),
            meta: {
              title: '添加接口权限',
              // noKeepAlive: true,
            },
          },
          {
            path: '/editApi',
            name: 'editApi',
            hidden: true,
            component: () => import('@/views/permissions/api/edit'),
            meta: {
              title: '编辑接口权限',
              noKeepAlive: true,
            },
          },
          {
            path: '/routeList',
            name: 'routeList',
            component: () => import('@/views/permissions/route/index'),
            meta: {
              title: '路由列表',
              noKeepAlive: true,
            },
          },
          {
            path: '/addRoute',
            name: 'addRoute',
            hidden: true,
            component: () => import('@/views/permissions/route/add'),
            meta: {
              title: '添加路由',
              noKeepAlive: true,
            },
          },
          {
            path: '/editRoute',
            name: 'editRoute',
            hidden: true,
            component: () => import('@/views/permissions/route/edit'),
            meta: {
              title: '编辑路由',
              noKeepAlive: true,
            },
          },
        ],
      },
    ],
  },
  {
    path: '/system',
    component: Layout,
    redirect: '/visitLog',
    alwaysShow: true,
    meta: {
      title: '系统',
      icon: 'desktop',
      fullTitle: '系统管理',
    },
    children: [
      {
        path: '/log-t',
        component: SubLayout,
        meta: {
          title: '日志',
        },
        children: [
          {
            path: '/visitLog',
            name: 'visitLog',
            component: () => import('@/views/system/visitLogs/index'),
            meta: {
              title: '访问日志',
              noKeepAlive: true,
            },
          },
          // {
          //   path: '/errLogList',
          //   name: 'errLogList',
          //   component: () => import('@/views/system/errLogs/index'),
          //   meta: {
          //     title: '错误日志',
          //     noKeepAlive: true,
          //   },
          // },
        ],
      },
    ],
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true,
  },
]

const router = new VueRouter({
  mode: routerMode,
  scrollBehavior: () => ({
    y: 0,
  }),
  routes: constantRoutes,
})
//注释的地方是允许路由重复点击，如果你觉得框架路由跳转规范太过严格可选择放开
/*const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};*/

export function resetRouter() {
  router.matcher = new VueRouter({
    mode: routerMode,
    scrollBehavior: () => ({
      y: 0,
    }),
    routes: constantRoutes,
  }).matcher
}

export default router
